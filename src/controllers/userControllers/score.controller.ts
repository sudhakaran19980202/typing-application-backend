// Libraries
import { Request, Response } from "express";

// Validation
import { createScoreValidation } from "../../validations/userValidations/score.validation";
import validation from "../../services/validation.service";

// DB Queries
import { createScoreQuery, findScoresQuery } from "../../db/queries/scores.query";


const listScores = async (req: Request, res: Response) => {
    const user = req.user;

    try {
        const scoreResponse = await findScoresQuery(user._id, 9); // passing user id and page limit
        return res.status(200).json({
            data: scoreResponse,
        });
    } catch (error) {
        return res.status(500).json({
            message: "Server error",
        });
    }
};

const createScore = async (req: Request, res: Response) => {
    const data = req.body;
    const user = req.user;

    // validate the request body
    const error = await validation(createScoreValidation, data);
    if (Object.keys(error).length !== 0) {
        return res.status(400).json({
            message: `${error[0].dataPath} ${error[0].message}`,
        });
    }

    try {
        // create a score
        await createScoreQuery(user._id, data);

        return res.status(204).send();
    } catch (error) {
        return res.status(500).json({
            message: "Server error",
        });
    }
};

export {
    listScores,
    createScore,
};