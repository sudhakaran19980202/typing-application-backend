// Libraries
import { Request, Response } from "express";

// Validation
import { updateProfileValidation } from "../../validations/userValidations/profile.validation";
import validation from "../../services/validation.service";

// DB Queries
import { updateUserQuery } from "../../db/queries/users.query";


const getProfile = async (req: Request, res: Response) => {
    const user = req.user;
    return res.status(200).json({
        data: {
            _id: user._id,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            bio: user.bio,
        },
    });
};

const updateProfile = async (req: Request, res: Response) => {
    const data = req.body;
    const user = req.user;

    // validate the request body
    const error = await validation(updateProfileValidation, data);
    if (Object.keys(error).length !== 0) {
        return res.status(400).json({
            message: `${error[0].dataPath} ${error[0].message}`,
        });
    }

    try {
        await updateUserQuery(user, data); // Update the profile details
        return res.status(200).json({
            message: "Profile updated successfully!",
        });
    } catch (error) {
        return res.status(500).json({
            message: "Server error",
        });
    }
};

export {
    getProfile,
    updateProfile,
};