// Libraries
import { Request, Response } from "express";

// Validation
import { inviteUserValidation } from "../../validations/commonValidations/invitation.validation";
import validation from "../../services/validation.service";

// Utils
import { sendMail } from "../../utils/sendMail.utils";

const inviteUser = async (req: Request, res: Response) => {
    const { email } = req.body;

    // validate the request body
    const error = await validation(inviteUserValidation, { email });
    if (Object.keys(error).length !== 0) {
        return res.status(400).json({
            message: `${error[0].dataPath} ${error[0].message}`,
        });
    }

    try {
        await sendMail(email);
        return res.status(204).send();
    } catch (error) {
        return res.status(500).json({
            message: "Server error",
        });
    }
};

export {
    inviteUser,
};