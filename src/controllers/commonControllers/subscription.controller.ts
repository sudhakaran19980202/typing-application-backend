// Libraries
import { Request, Response } from "express";

// Validation
import { createSubscriptionValidation } from "../../validations/commonValidations/subscription.validation";
import validation from "../../services/validation.service";

// DB Queries
import { createSubscriptionQuery, findSubscriptionQuery } from "../../db/queries/subscriptions.query";


const createSubscription = async (req: Request, res: Response) => {
    const { email } = req.body;

    // validate the request body
    const error = await validation(createSubscriptionValidation, { email });
    if (Object.keys(error).length !== 0) {
        return res.status(400).json({
            message: `${error[0].dataPath} ${error[0].message}`,
        });
    }

    try {
        const existingEmail = await findSubscriptionQuery(email);
        if (!existingEmail) {
            await createSubscriptionQuery(email);
        }
        return res.status(204).send();
    } catch (error) {
        return res.status(500).json({
            message: "Server error",
        });
    }
};

export {
    createSubscription,
};