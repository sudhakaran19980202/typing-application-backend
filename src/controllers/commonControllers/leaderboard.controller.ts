// Libraries
import { Request, Response } from "express";

// DB Queries
import { getLeaderboardQuery } from "../../db/queries/scores.query";


const getLeaderboard = async (_req: Request, res: Response) => {
    try {
        const pageLimit = 10;
        const leaderboardDetails = await getLeaderboardQuery(pageLimit);
        return res.status(200).json({
            data: leaderboardDetails,
        });

    } catch (error) {
        return res.status(500).json({
            message: "Server error",
        });
    }
};


export {
    getLeaderboard,
};