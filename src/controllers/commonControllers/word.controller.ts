// Libraries
import { Request, Response } from "express";

// DB Queries
import { createWordsQuery, findAllWordsQuery, findWordQuery } from "../../db/queries/words.query";


const getWordsList = async (_req: Request, res: Response) => {
    const sampleLimit = 100;
    try {
        const wordsList = await findWordQuery(sampleLimit);
        return res.status(200).json({
            data: wordsList,
        });
    } catch (error) {
        return res.status(500).json({
            message: "Server error",
        });
    }
};

const migrateWords = async (req: Request, res: Response) => {
    const newWords: string[] = req.body.words;
    try {
        const existingWords = await findAllWordsQuery();
        const uniqueNewWords: string[] = newWords.filter((word) => !existingWords.includes(word.toLowerCase()));
        if (uniqueNewWords.length) {
            await createWordsQuery(uniqueNewWords);
        }

        return res.status(200).json({ message: "Words migrated successfully!" });
    } catch (error) {
        return res.status(500).json({
            message: "Server error",
        });
    }
};

export {
    getWordsList,
    migrateWords,
};