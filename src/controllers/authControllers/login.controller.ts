// Libraries
import {Request, Response} from "express";

// Validation
import loginValidation from "../../validations/authValidations/login.validation";
import validation from "../../services/validation.service";

// Utils
import generateToken from "../../utils/generateToken.utils";


const login = async (req: Request, res: Response) => {
    const data = req.body;

    // validate the request body
    const error = await validation(loginValidation, data);
    if (Object.keys(error).length !== 0) {
        return res.status(400).json({
            message: `${error[0].dataPath} ${error[0].message}`,
        });
    }

    try {
        const user = req.user;
        const token = generateToken(user._id);
        return res.status(200).json({
            message: "Loggedin successfully!",
            token,
            data: {
                _id: user._id,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                bio: user.bio,
                isEnabled: user.isEnabled,
            },
        });
    } catch (error) {
        return res.status(500).json({
            message: "Server error",
        });
    }
};

export {
    login,
};