// Libraries
import { Request, Response } from "express";
import bcrypt from "bcryptjs";

// Validation
import signupValidation from "../../validations/authValidations/signup.validation";
import validation from "../../services/validation.service";

// DB Queries
import { findUserQuery, createUserQuery } from "../../db/queries/users.query";

// Utils
import generateToken from "../../utils/generateToken.utils";


const signup = async (req: Request, res: Response) => {
    const data = req.body;

    // validate the request body
    const validationError = await validation(signupValidation, data);
    if (Object.keys(validationError).length !== 0) {
        return res.status(400).json({
            message: `${validationError[0].dataPath} ${validationError[0].message}`,
        });
    }

    // check the password and confirm password
    if (data.password !== data.confirmPassword) {
        return res.status(400).json({
            message: "Password is mismatched",
        });
    }

    try {
        // Check the existing user
        const existingUser = await findUserQuery(data.email);
        if (existingUser) {
            return res.status(404).json({
                message: "Email already exists!",
            });
        }

        const salt: string = bcrypt.genSaltSync(10);
        const hash: string = bcrypt.hashSync(data.password, salt);
        const user = await createUserQuery(data, hash); // create a new user

        const token = generateToken(user._id);
        return res.status(200).json({
            message: "Registered successfully!",
            token,
        });
    } catch (error) {
        return res.status(500).json({
            message: "Server error",
        });
    }
};

export {
    signup,
};