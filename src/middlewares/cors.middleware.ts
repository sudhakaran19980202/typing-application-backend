
// type stringAndArray = string | string[]
interface cors {
    origins: string | string[],
    methods: string | string[],
    allowedHeaders: string | string[],
    maxAge?: number | null
}
const corsOptions: cors = {
    origins: "*",
    methods: "*",
    allowedHeaders: "*",
    maxAge: 1892160000, // 1 year
};

export { corsOptions };