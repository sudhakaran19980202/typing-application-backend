// Passport Libraries
import passport from "passport";
import passportJwt from "passport-jwt";
import LocalStrategy from "passport-local";

// Models
import User from "../models/users.model";

// Utils
import { bcryptCompare } from "../utils/bcrypt.utils";

// Access .env variables
// dotenv.config({ path: path.join(__dirname, `./.env.${process.env['NODE_ENV']}`)})

// JWT token verification
const JwtStrategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;
const jwtVerify = {
    jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderWithScheme("JWT"),
        ExtractJwt.fromUrlQueryParameter("token"),
    ]),
    secretOrKey: process.env['SECRET_KEY'],
};

// Autenticate User
const userOption = {
    usernameField: "email",
    passwordField: "password",
};
const authenticateUser = new LocalStrategy(userOption, async function (email: string, password: string, done) {
    const user = await User.findOne({ email, isEnabled: true });
    if (!user) {
        return done(null, false);
    }
    const userDetails = user['_doc'];
    // compare the password
    const isMatched: boolean = await bcryptCompare(password, user.password);
    if (isMatched) {
        return done(null, { ...userDetails, role: "user" });
    } else {
        return done(null, false);
    }
});

const authorizeUser = new JwtStrategy(jwtVerify, async function (payload: { _id: string }, done) {
    const user = await User.findOne({ _id: payload._id, isEnabled: true });
    if (!user) {
        return done(null, false);
    }

    const userDetails = user['_doc'];
    return done(null, { ...userDetails });
});

passport.use("authenticate-user", authenticateUser);
passport.use("authorize-user", authorizeUser);