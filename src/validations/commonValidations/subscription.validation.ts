const createSubscriptionValidation = {
    type: "object",
    additionalProperties: false,
    required: [
        "email",

    ],
    properties: {
        email: {
            type: "string",
        },
    },
};

export { createSubscriptionValidation };