const loginValidation = {
    type: "object",
    additionalProperties: false,
    required: [
        "email",
        "password",
    ],
    properties: {
        email: {
            type: "string",
        },
        password: {
            type: "string",
            length: {
                min: 8,
                max: 16,
            },
        },
    },
};

export = loginValidation