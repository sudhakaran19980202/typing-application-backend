const signupValidation = {
    type: "object",
    additionalProperties: false,
    required: [
        "firstName",
        "lastName",
        "email",
        "password",
        "confirmPassword",
        "bio",
    ],
    properties: {
        firstName: {
            type: "string",
        },
        lastName: {
            type: "string",
        },
        email: {
            type: "string",
        },
        password: {
            type: "string",
        },
        confirmPassword: {
            type: "string",
        },
        bio: {
            type: "string",
        },
    },
};

export = signupValidation