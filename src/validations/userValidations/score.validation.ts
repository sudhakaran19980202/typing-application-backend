const createScoreValidation = {
    type: "object",
    additionalProperties: false,
    required: [
        "wpm",
        "cpm",
        "accuracy",
        "type",
    ],
    properties: {
        wpm: {
            type: "number",
            length: {
                min: 0,
            },
        },
        cpm: {
            type: "number",
            length: {
                min: 0,
            },
        },
        accuracy: {
            type: "number",
            length: {
                min: 0,
                max: 100,
            },
        },
        type: {
            type: "string",
            enum: ["Rabbit", "Turtle", "Sloth"],
        },
    },
};

export { createScoreValidation };