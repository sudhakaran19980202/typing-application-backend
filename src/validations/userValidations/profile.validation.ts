const updateProfileValidation = {
    type: "object",
    additionalProperties: false,
    required: [],
    properties: {
        firstName: {
            type: "string",
        },
        lastName: {
            type: "string",
        },
        bio: {
            type: "string",
        },
        profileUrl: {
            type: "string",
        },
    },
};

export { updateProfileValidation };