// Env config
import "./config/environment.config";

// Libraries
import express, { Express } from "express";
import cors from "cors";

// Middleware
import { corsOptions } from "./middlewares/cors.middleware";

// Routes
import { authRouter, userRouter, commonRouter } from "./routes";

// Db config
import "./config/db.config";

console.log(process.env['MONGO_DB'], "???");

const app: Express = express();

app.use(express.json());
app.use(cors(corsOptions));

app.use("/auth", authRouter);
app.use("/user", userRouter);
app.use("/common", commonRouter);

const PORT = process.env['SERVER_PORT'];
app.listen(PORT, () => {
    console.log(`Server running at PORT: ${PORT}`);
});
