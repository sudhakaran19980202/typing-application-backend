declare module "cors" {
    import express from "express";

    interface CorsOptions {
        origin?: string | string[];
        methods?: string | string[];
        allowedHeaders?: string | string[];
        maxAge?: number | null;
    }

    function cors(options?: CorsOptions): express.RequestHandler;

    export = cors;
}
