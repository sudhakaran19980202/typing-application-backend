import mongoose from "mongoose";

const MONGO_CONNECTION = process.env['MONGO_CONNECTION'];
const MONGO_USERNAME = process.env['MONGO_USERNAME'];
const MONGO_PASSWORD = process.env['MONGO_PASSWORD'];
const MONGO_HOSTNAME = process.env['MONGO_HOSTNAME'];
const MONGO_DB = process.env['MONGO_DB'];

let url: string;
if (MONGO_CONNECTION === "ATLAS") {
    // For using MongoDB Atlas
    url = `mongodb+srv://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}/${MONGO_DB}?authSource=admin`;
} else {
    // For Using Local DB
    const MONGO_PORT = process.env['MONGO_PORT'];
    url = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`;
}

mongoose.connect(url);

console.log(url, "::::::::::::::::::");
export default url;
