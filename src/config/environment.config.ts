import dotenv from "dotenv";

// Load environment-specific variables
const environment = process.env['NODE_ENV'];
const envFile = `.env.${environment}`;

// Change the file path based on the environemnt specific
dotenv.config({ path: envFile });
