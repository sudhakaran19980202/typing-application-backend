// Libraries
import { Router } from "express";

// Controllers
import { signup } from "../controllers/authControllers/signup.controller";
import { login } from "../controllers/authControllers/login.controller";

// Middleware
import passport from "passport";
import '../middlewares/authentication.middleware';

const authRouter = Router();

authRouter.post("/signup", signup);
authRouter.post("/login", passport.authenticate('authenticate-user', { session: false }), login);

export default authRouter;