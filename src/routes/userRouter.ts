// Libraries
import { Router } from "express";

// Controllers
import { getProfile, updateProfile } from "../controllers/userControllers/profile.controller";
import { listScores, createScore } from "../controllers/userControllers/score.controller";

// Middleware
import passport from "passport";
import '../middlewares/authentication.middleware';

const userRouter = Router();

userRouter.get("/profile-details", passport.authenticate('authorize-user', { session: false }), getProfile);
userRouter.put("/profile-details", passport.authenticate('authorize-user', { session: false }), updateProfile);

userRouter.get("/scores", passport.authenticate('authorize-user', { session: false }), listScores);
userRouter.post("/scores", passport.authenticate('authorize-user', { session: false }), createScore);

export default userRouter;