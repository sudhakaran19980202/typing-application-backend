import authRouter from "./authRouter";
import userRouter from "./userRouter";
import commonRouter from "./commonRouter";

export { authRouter, userRouter, commonRouter }; 