// Libraries
import { Router } from "express";

// Controllers
import { getLeaderboard } from "../controllers/commonControllers/leaderboard.controller";
import { getWordsList, migrateWords } from "../controllers/commonControllers/word.controller";
import { createSubscription } from "../controllers/commonControllers/subscription.controller";
import {inviteUser} from "../controllers/commonControllers/invitation.controller";

const commonRouter = Router();

// Leaderboard Route
commonRouter.get("/leaderboards", getLeaderboard);

// Words Route
commonRouter.get("/words/list", getWordsList);
commonRouter.post("/words/migrate", migrateWords);

// Subscription Route
commonRouter.post("/subscriptions", createSubscription);

// Invitation Route
commonRouter.post("/invite-user", inviteUser);

export default commonRouter;