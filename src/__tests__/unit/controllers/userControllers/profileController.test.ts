import "../../../../config/environment.config";

import { Request, Response } from 'express';
import { getProfile, updateProfile } from '../../../../controllers/userControllers/profile.controller';
import * as ValidationService from "../../../../services/validation.service";
import { updateUserQuery } from "../../../../db/queries/users.query";

// Mocking Data
const mockData = {
    user: {
        "_id": "650afb86dd259d8b07c201c1",
        "firstName": "Sudhakaran",
        "lastName": "K",
        "email": "sudhakaran@gmail.com",
        "password": "$2a$10$6zl4unS/bgsw1s2oiY2gFO8yAGTe7T9zBHaxE5mO1hLCpJIjlIZr2",
        "bio": "Software developer",
        "isEnabled": true,
    },
    getProfileResponse: {
        "_id": "650afb86dd259d8b07c201c1",
        "firstName": "Sudhakaran",
        "lastName": "K",
        "email": "sudhakaran@gmail.com",
        "bio": "Software developer",
    },
    requestBody: {
        "firstName": "Sudhakaran",
        "lastName": "K",
        "email": "sudhakaran@gmail.com",
        "bio": "Software developer",
    },
    updateQueryResponse: {

    },
};

// Mocking a request and response object
const mockRequest = () => {
    const req: Partial<Request> = {};
    req.body = {};
    req.user = mockData.user;
    return req as Request;
};

const mockResponse = () => {
    const res: Partial<Response> = {};
    res.status = jest.fn().mockReturnThis();
    res.json = jest.fn().mockReturnThis();
    return res as Response;
};

// Mocking Modules
jest.mock("../../../../db/queries/users.query", () => ({
    __esModule: true,
    updateUserQuery: jest.fn(),
}));


describe('Profile function', () => {

    describe('Get Profile function', () => {
        test('should response with success message', async () => {
            const req = mockRequest();
            const res = mockResponse();

            // Call the profile function
            await getProfile(req, res);

            const expectedResponse = Object.assign(mockData?.getProfileResponse);

            // Assert profile function response
            expect(res.status).toHaveBeenCalledWith(200);
            expect(res.json).toHaveBeenCalledWith({
                data: expectedResponse,
            });
        });
    });

    describe('Update Profile Function', () => {
        let mockValidationService: jest.SpyInstance;

        beforeAll(() => {
            // Mocking dependency functions
            mockValidationService = jest.spyOn(ValidationService, 'default');
        });

        afterEach(() => {
            /*
              - Reset the value and the calls history after each test cases
              - To assert the mocked functions whether the calls happening on each test cases or not
            */
            mockValidationService.mockReset();
            (updateUserQuery as jest.Mock).mockReset();
        });

        afterAll(() => {
            // Restore to the original implementation after all the test cases have been done 
            mockValidationService.mockRestore();
        });

        test('should response with success message', async () => {
            const req = mockRequest();
            const res = mockResponse();
            req.body = mockData.requestBody;

            mockValidationService.mockReturnValue({});
            (updateUserQuery as jest.Mock).mockResolvedValue(null);

            // Call the profile function
            await updateProfile(req, res);

            // Assert profile function response
            expect(res.status).toHaveBeenCalledWith(200);
            expect(res.json).toHaveBeenCalledWith({
                message: "Profile updated successfully!",
            });
        });

        test('should respond with a server error on exception', async () => {
            const req = mockRequest();
            const res = mockResponse();
            req.body = mockData.requestBody;

            mockValidationService.mockReturnValue({});

            // Mocking an exception
            (updateUserQuery as jest.Mock).mockImplementation(() => { throw new Error('Error while querying data'); });

            await updateProfile(req, res);

            // Assert profile function response
            expect(updateUserQuery).toThrow();
            expect(res.status).toHaveBeenCalledWith(500);
            expect(res.json).toHaveBeenCalledWith({
                message: 'Server error',
            });
        });
    });
});
