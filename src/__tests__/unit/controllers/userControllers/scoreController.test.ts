import "../../../../config/environment.config";

import { Request, Response } from 'express';
import { createScore, listScores } from '../../../../controllers/userControllers/score.controller';
import * as ValidationService from "../../../../services/validation.service";
import { findScoresQuery, createScoreQuery } from "../../../../db/queries/scores.query";

// Mocking Data
const mockData = {
    user: {
        "_id": "650afb86dd259d8b07c201c1",
        "firstName": "Sudhakaran",
        "lastName": "K",
        "email": "sudhakaran@gmail.com",
        "password": "$2a$10$6zl4unS/bgsw1s2oiY2gFO8yAGTe7T9zBHaxE5mO1hLCpJIjlIZr2",
        "bio": "Software developer",
        "isEnabled": true,
    },
    listScores: [
        {
            "_id": "650afb86dd259d8b07c201d2",
            "userId": "650afb86dd259d8b07c201c1",
            "wpm": 40,
            "cpm": 170,
            "accuracy": 89,
            "type": "Rabbit",
        },
        {
            "_id": "650afb86dd259d8b07c201d1",
            "userId": "650afb86dd259d8b07c201c1",
            "wpm": 30,
            "cpm": 170,
            "accuracy": 87,
            "type": "Turtle",
        },
    ],
    requestBody: {
        "wpm": 30,
        "cpm": 170,
        "accuracy": 87,
        "type": "Turtle",
    },
};

// Mocking a request and response object
const mockRequest = () => {
    const req: Partial<Request> = {};
    req.body = {};
    req.user = mockData.user;
    return req as Request;
};

const mockResponse = () => {
    const res: Partial<Response> = {};
    res.status = jest.fn().mockReturnThis();
    res.json = jest.fn().mockReturnThis();
    return res as Response;
};

// Mocking Modules
jest.mock("../../../../db/queries/scores.query", () => ({
    __esModule: true,
    findScoresQuery: jest.fn(),
    createScoreQuery: jest.fn(),
}));


describe('Score function', () => {

    describe('Get Score function', () => {
        test('should response with success message', async () => {
            const req = mockRequest();
            const res = mockResponse();

            (findScoresQuery as jest.Mock).mockResolvedValue(mockData.listScores);

            // Call the scores function
            await listScores(req, res);

            const expectedResponse = Object.assign(mockData?.listScores);

            // Assert scores function response
            expect(res.status).toHaveBeenCalledWith(200);
            expect(res.json).toHaveBeenCalledWith({
                data: expectedResponse,
            });
        });

        test('should respond with a server error on exception', async () => {
            const req = mockRequest();
            const res = mockResponse();

            // Mocking an exception
            (findScoresQuery as jest.Mock).mockImplementation(() => { throw new Error('Error while querying data'); });

            await listScores(req, res);

            // Assert scores function response
            expect(findScoresQuery).toThrow();
            expect(res.status).toHaveBeenCalledWith(500);
            expect(res.json).toHaveBeenCalledWith({
                message: 'Server error',
            });
        });
    });

    describe('Create Score Function', () => {
        let mockValidationService: jest.SpyInstance;

        beforeAll(() => {
            // Mocking dependency functions
            mockValidationService = jest.spyOn(ValidationService, 'default');
        });

        afterEach(() => {
            /*
              - Reset the value and the calls history after each test cases
              - To assert the mocked functions whether the calls happening on each test cases or not
            */
            mockValidationService.mockReset();
            (createScoreQuery as jest.Mock).mockReset();
        });

        afterAll(() => {
            // Restore to the original implementation after all the test cases have been done 
            mockValidationService.mockRestore();
        });

        test('should response with success message', async () => {
            const req = mockRequest();
            const res = mockResponse();
            req.body = mockData.requestBody;

            mockValidationService.mockReturnValue({});
            (createScoreQuery as jest.Mock).mockResolvedValue(null);

            // Call the score function
            await createScore(req, res);

            // Assert scores function response
            expect(res.status).toHaveBeenCalledWith(204);
        });

        test('should respond with a server error on exception', async () => {
            const req = mockRequest();
            const res = mockResponse();
            req.body = mockData.requestBody;

            mockValidationService.mockReturnValue({});

            // Mocking an exception
            (createScoreQuery as jest.Mock).mockImplementation(() => { throw new Error('Error while querying data'); });

            await createScore(req, res);

            // Assert scores function response
            expect(createScoreQuery).toThrow();
            expect(res.status).toHaveBeenCalledWith(500);
            expect(res.json).toHaveBeenCalledWith({
                message: 'Server error',
            });
        });
    });
});
