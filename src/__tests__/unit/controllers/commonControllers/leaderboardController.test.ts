import "../../../../config/environment.config";

import { Request, Response } from 'express';
import { getLeaderboard } from '../../../../controllers/commonControllers/leaderboard.controller';
import { getLeaderboardQuery } from "../../../../db/queries/scores.query";

// Mocking Data
const mockData = {
    leaderboard: [
        {
            "_id": "650afb86dd259d8b07c201d2",
            "userId": {
                "firstName": "Sudhakaran",
                "lastName": "K",
            },
            "wpm": 40,
            "cpm": 170,
            "accuracy": 89,
        },
        {
            "_id": "650afb86dd259d8b07c201d1",
            "userId": {
                "firstName": "Sudhakaran",
                "lastName": "K",
            },
            "wpm": 30,
            "cpm": 170,
            "accuracy": 87,
        },
    ],
};

// Mocking a request and response object
const mockRequest = () => {
    const req: Partial<Request> = {};
    req.body = {};
    req.user = {};
    return req as Request;
};

const mockResponse = () => {
    const res: Partial<Response> = {};
    res.status = jest.fn().mockReturnThis();
    res.json = jest.fn().mockReturnThis();
    return res as Response;
};

// Mocking Modules
jest.mock("../../../../db/queries/scores.query", () => ({
    __esModule: true,
    getLeaderboardQuery: jest.fn(),
}));


describe('Leaderboard function', () => {

    afterEach(() => {
        // Reset the value and the calls history after each test cases
        (getLeaderboardQuery as jest.Mock).mockReset();
    });

    test('should respond with a success message', async () => {

        const req = mockRequest();
        const res = mockResponse();

        (getLeaderboardQuery as jest.Mock).mockResolvedValue(mockData.leaderboard);

        // Call the leaderboard function
        await getLeaderboard(req, res);

        const expectedResponse = Object.assign(mockData?.leaderboard);

        // Assert leaderboard function response
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({
            data: expectedResponse,
        });
    });

    test('should respond with a server error on exception', async () => {
        const req = mockRequest();
        const res = mockResponse();

        // Mocking an exception
        (getLeaderboardQuery as jest.Mock).mockImplementation(() => { throw new Error('Error while querying data'); });

        await getLeaderboard(req, res);

        // Assert leaderboard function response
        expect(getLeaderboardQuery).toThrow();
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({
            message: 'Server error',
        });
    });
});