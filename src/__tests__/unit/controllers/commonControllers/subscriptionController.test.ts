import "../../../../config/environment.config";

import { Request, Response } from 'express';
import { createSubscription } from '../../../../controllers/commonControllers/subscription.controller';
import * as ValidationService from "../../../../services/validation.service";
import { findSubscriptionQuery, createSubscriptionQuery } from "../../../../db/queries/subscriptions.query";

// Mocking Data
const mockData = {
    requestBody: {
        "email": "sudhakaran@gmail.com",
    },
    findQueryResponse: {
        "_id": "650afb86dd259d8b07c201c1",
        "email": "sudhakaran@gmail.com",
        "createdAt": "2023-01-01T12:00:00",
        "updatedAt": "2023-01-01T12:00:00",
    },
    passwordValidationError: [
        {
            keyword: 'type',
            dataPath: '.email',
            schemaPath: '#/properties/email/type',
            params: { type: 'string' },
            message: 'should be string',
        },
    ],
};

// Mocking a request and response object
const mockRequest = () => {
    const req: Partial<Request> = {};
    req.body = {};
    return req as Request;
};

const mockResponse = () => {
    const res: Partial<Response> = {};
    res.status = jest.fn().mockReturnThis();
    res.json = jest.fn().mockReturnThis();
    return res as Response;
};


// Mocking Modules
jest.mock("../../../../db/queries/subscriptions.query", () => ({
    __esModule: true,
    findSubscriptionQuery: jest.fn(),
    createSubscriptionQuery: jest.fn(),
}));


describe('signup/register function', () => {

    let mockValidationService: jest.SpyInstance;

    beforeAll(() => {
        // Mocking dependency functions
        mockValidationService = jest.spyOn(ValidationService, 'default');
    });

    afterEach(() => {
        /*
          - Reset the value and the calls history after each test cases
          - To assert the mocked functions whether the calls happening on each test cases or not
        */
        mockValidationService.mockReset();
        (findSubscriptionQuery as jest.Mock).mockReset();
        (createSubscriptionQuery as jest.Mock).mockReset();
    });

    afterAll(() => {
        // Restore to the original implementation after all the test cases have been done 
        mockValidationService.mockRestore();
    });

    test('should respond with a success message', async () => {
        const req = mockRequest();
        const res = mockResponse();
        req.body = mockData.requestBody;

        mockValidationService.mockReturnValue({});
        (findSubscriptionQuery as jest.Mock).mockResolvedValue(null);
        (createSubscriptionQuery as jest.Mock).mockResolvedValue(null);

        await createSubscription(req, res);

        const expectedResponse: any = { ...mockData.requestBody };
        delete expectedResponse.password;

        // Assert whether the dependency functions has been called
        expect(mockValidationService).toHaveBeenCalledTimes(1);

        // Assert subscription function response
        expect(res.status).toHaveBeenCalledWith(204);
    });

    test('should respond with a validation error on invalid input', async () => {
        const req = mockRequest();
        const res = mockResponse();
        const requestBody = { email: 12345678 };
        req.body = requestBody;

        // Assuming validation function returns an error object for invalid input
        mockValidationService.mockReturnValue(mockData.passwordValidationError);
        (findSubscriptionQuery as jest.Mock).mockResolvedValue(null);
        (createSubscriptionQuery as jest.Mock).mockResolvedValue(null);

        await createSubscription(req, res);

        // Assert whether the dependency functions has been called
        expect(mockValidationService).toHaveBeenCalledTimes(1);

        // Assert subscription function response
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            message: '.email should be string',
        });
    });

    test('should respond with a server error on exception', async () => {
        const req = mockRequest();
        const res = mockResponse();
        req.body = mockData.requestBody;

        // Mocking an exception
        mockValidationService.mockReturnValue({});
        (findSubscriptionQuery as jest.Mock).mockImplementation(() => { throw new Error('Error while querying data'); });

        await createSubscription(req, res);

        // Assert whether the dependency functions has been called
        expect(mockValidationService).toHaveBeenCalledTimes(1);

        // Assert subscription function response
        expect(findSubscriptionQuery).toThrow();
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({
            message: 'Server error',
        });
    });
});