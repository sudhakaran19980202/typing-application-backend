import "../../../../config/environment.config";

import { Request, Response } from 'express';
import { getWordsList } from '../../../../controllers/commonControllers/word.controller';
import { findWordQuery } from "../../../../db/queries/words.query";

// Mocking Data
const mockData = {
    findQueryResponse: [
        "river",
        "coral",
        "understand",
        "grow",
        "ponder",
        "swim",
        "cold",
        "cherry",
        "early",
        "thrive",
        "far",
        "wish",
        "live",
        "laugh",
        "heavy",
        "space",
        "play",
        "sing",
        "vast",
        "drive",
        "pearl",
        "grow",
        "dim",
        "cloud",
        "sour",
        "run",
        "open",
        "falcon",
        "smile",
        "neat",
        "dirty",
        "beach",
        "think",
        "smell",
        "go",
        "take",
        "dream",
        "star",
        "tasty",
        "dark",
        "cry",
        "umbrella",
        "cool",
        "igloo",
        "walk",
        "rose",
        "cat",
        "think",
        "mint",
        "succeed",
        "sing",
        "drive",
        "reflect",
        "be",
        "globe",
        "pray",
        "wild",
        "smart",
        "teach",
        "happy",
        "break",
        "loud",
        "fly",
        "discover",
        "close",
        "full",
        "forget",
        "fail",
        "jump",
        "shout",
        "strong",
        "fast",
        "black",
        "spicy",
        "noodle",
        "slow",
        "wait",
        "near",
        "high",
        "play",
        "young",
        "search",
        "forgive",
        "drink",
        "hide",
        "believe",
        "remember",
        "smile",
        "foggy",
        "fresh",
        "understand",
        "apple",
        "ocean",
        "brave",
        "dog",
        "forget",
        "chilly",
        "dull",
        "scared",
        "struggle",
    ],
};

// Mocking a request and response object
const mockRequest = () => {
    const req: Partial<Request> = {};
    req.body = {};
    req.user = {};
    return req as Request;
};

const mockResponse = () => {
    const res: Partial<Response> = {};
    res.status = jest.fn().mockReturnThis();
    res.json = jest.fn().mockReturnThis();
    return res as Response;
};

// Mocking Modules
jest.mock("../../../../db/queries/words.query", () => ({
    __esModule: true,
    findWordQuery: jest.fn(),
}));


describe('Words function', () => {

    afterEach(() => {
        // Reset the value and the calls history after each test cases
        (findWordQuery as jest.Mock).mockReset();
    });

    test('should respond with a success message', async () => {

        const req = mockRequest();
        const res = mockResponse();

        (findWordQuery as jest.Mock).mockResolvedValue(mockData.findQueryResponse);

        // Call the words function
        await getWordsList(req, res);

        const expectedResponse = Object.assign(mockData?.findQueryResponse);

        // Assert words function response
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({
            data: expectedResponse,
        });
    });

    test('should respond with a server error on exception', async () => {
        const req = mockRequest();
        const res = mockResponse();

        // Mocking an exception
        (findWordQuery as jest.Mock).mockImplementation(() => { throw new Error('Error while querying data'); });

        await getWordsList(req, res);

        // Assert words function response
        expect(findWordQuery).toThrow();
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({
            message: 'Server error',
        });
    });
});