import "../../../../config/environment.config";

import { Request, Response } from 'express';
import { login } from '../../../../controllers/authControllers/login.controller';
import * as ValidationService from "../../../../services/validation.service";
import * as GenerateToken from "../../../../utils/generateToken.utils";

// Mocking Data
const mockData = {
    user: {
        "_id": "650afb86dd259d8b07c201c1",
        "firstName": "Sudhakaran",
        "lastName": "K",
        "email": "sudhakaran@gmail.com",
        "password": "$2a$10$6zl4unS/bgsw1s2oiY2gFO8yAGTe7T9zBHaxE5mO1hLCpJIjlIZr2",
        "bio": "Software developer",
        "isEnabled": true,
    },
    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
    passwordValidationError: [
        {
            keyword: 'type',
            dataPath: '.password',
            schemaPath: '#/properties/password/type',
            params: { type: 'string' },
            message: 'should be string',
        },
    ],
};

// Mocking a request and response object
const mockRequest = () => {
    const req: Partial<Request> = {};
    req.body = {};
    req.user = mockData.user;
    return req as Request;
};

const mockResponse = () => {
    const res: Partial<Response> = {};
    res.status = jest.fn().mockReturnThis();
    res.json = jest.fn().mockReturnThis();
    return res as Response;
};

describe('login function', () => {

    let mockValidationService: jest.SpyInstance;
    let mockGenerateToken: jest.SpyInstance;

    beforeAll(() => {
    // Mocking dependency functions
        mockValidationService = jest.spyOn(ValidationService, 'default');
        mockGenerateToken = jest.spyOn(GenerateToken, 'default');
    });

    afterEach(() => {
    /*
      - Reset the value and the calls history after each test cases
      - To assert the mocked functions whether the calls happening on each test cases or not
    */
        mockValidationService.mockReset();
        mockGenerateToken.mockReset();
    });

    afterAll(() => {
    // Restore to the original implementation after all the test cases have been done 
        mockValidationService.mockRestore();
        mockGenerateToken.mockRestore();
    });

    test('should respond with a success message', async () => {

        const req = mockRequest();
        const res = mockResponse();
        req.body = { email: "sudhakaran19980202@gmail.com", password: "password" };

        mockGenerateToken.mockReturnValue(mockData.token);
        mockValidationService.mockReturnValue({});

        // Call the login function
        await login(req, res);

        const expectedResponse = Object.assign(mockData?.user);
        delete expectedResponse.password;

        // Assert whether the dependency functions has been called
        expect(mockValidationService).toHaveBeenCalledTimes(1);
        expect(mockGenerateToken).toHaveBeenCalledTimes(1);

        // Assert login function response
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({
            message: 'Loggedin successfully!',
            token: mockData.token,
            data: expectedResponse,
        });
    });

    test('should respond with a validation error on invalid input', async () => {
        const req = mockRequest();
        const res = mockResponse();
        req.body = { email: "sudhakaran19980202@gmail.com", password: 12345678 };

        // Assuming validation function returns an error object for invalid input
        mockGenerateToken.mockReturnValue(mockData.token);
        mockValidationService.mockReturnValue(mockData.passwordValidationError);

        await login(req, res);

        // Assert whether the dependency functions has been called
        expect(mockValidationService).toHaveBeenCalledTimes(1);
        expect(mockGenerateToken).toHaveBeenCalledTimes(0);

        // Assert login function response
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            message: '.password should be string',
        });
    });

    test('should respond with a server error on exception', async () => {
        const req = mockRequest();
        const res = mockResponse();
        req.body = { email: "sudhakaran19980202@gmail.com", password: "password" };

        // Mocking an exception
        mockGenerateToken.mockImplementation(() => { throw new Error('Error while generating token'); });
        mockValidationService.mockReturnValue({});

        await login(req, res);

        // Assert whether the dependency functions has been called
        expect(mockValidationService).toHaveBeenCalledTimes(1);
        expect(mockGenerateToken).toHaveBeenCalledTimes(1);

        // Assert login function response
        expect(mockGenerateToken).toThrow();
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({
            message: 'Server error',
        });
    });
});