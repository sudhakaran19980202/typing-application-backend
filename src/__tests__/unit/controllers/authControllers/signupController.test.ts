import "../../../../config/environment.config";

import { Request, Response } from 'express';
import { signup } from '../../../../controllers/authControllers/signup.controller';
import * as ValidationService from "../../../../services/validation.service";
import * as GenerateToken from "../../../../utils/generateToken.utils";
import { findUserQuery, createUserQuery } from "../../../../db/queries/users.query";

// Mocking Data
const mockData = {
    requestBody: {
        "firstName": "Sudhakaran",
        "lastName": "K",
        "email": "sudhakaran@gmail.com",
        "password": "password",
        "confirmPassword": "password",
        "bio": "Software developer",
    },
    findQueryResponse: {
        "_id": "650afb86dd259d8b07c201c1",
        "firstName": "Sudhakaran",
        "lastName": "K",
        "email": "sudhakaran@gmail.com",
        "bio": "Software developer",
        "isEnabled": true,
        "createdAt": "2023-01-01T12:00:00",
        "updatedAt": "2023-01-01T12:00:00",
    },
    createQueryResponse: {
        "_id": "650afb86dd259d8b07c201c1",
    },
    token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
    passwordValidationError: [
        {
            keyword: 'type',
            dataPath: '.password',
            schemaPath: '#/properties/password/type',
            params: { type: 'string' },
            message: 'should be string',
        },
    ],
};

// Mocking a request and response object
const mockRequest = () => {
    const req: Partial<Request> = {};
    req.body = {};
    return req as Request;
};

const mockResponse = () => {
    const res: Partial<Response> = {};
    res.status = jest.fn().mockReturnThis();
    res.json = jest.fn().mockReturnThis();
    return res as Response;
};


// Mocking Modules
jest.mock("../../../../db/queries/users.query", () => ({
    __esModule: true,
    findUserQuery: jest.fn(),
    createUserQuery: jest.fn(),
}));


describe('signup/register function', () => {

    let mockValidationService: jest.SpyInstance;
    let mockGenerateToken: jest.SpyInstance;

    beforeAll(() => {
        // Mocking dependency functions
        mockValidationService = jest.spyOn(ValidationService, 'default');
        mockGenerateToken = jest.spyOn(GenerateToken, 'default');
    });

    afterEach(() => {
        /*
          - Reset the value and the calls history after each test cases
          - To assert the mocked functions whether the calls happening on each test cases or not
        */
        mockValidationService.mockReset();
        mockGenerateToken.mockReset();
        (findUserQuery as jest.Mock).mockReset();
        (createUserQuery as jest.Mock).mockReset();
    });

    afterAll(() => {
        // Restore to the original implementation after all the test cases have been done 
        mockValidationService.mockRestore();
        mockGenerateToken.mockRestore();
    });

    test('should respond with a success message', async () => {
        const req = mockRequest();
        const res = mockResponse();
        req.body = mockData.requestBody;

        mockGenerateToken.mockReturnValue(mockData.token);
        mockValidationService.mockReturnValue({});
        (findUserQuery as jest.Mock).mockResolvedValue(null);
        (createUserQuery as jest.Mock).mockResolvedValue(mockData.createQueryResponse);

        await signup(req, res);

        const expectedResponse: any = { ...mockData.requestBody };
        delete expectedResponse.password;

        // Assert whether the dependency functions has been called
        expect(mockValidationService).toHaveBeenCalledTimes(1);
        expect(mockGenerateToken).toHaveBeenCalledTimes(1);

        // Assert signup function response
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({
            message: 'Registered successfully!',
            token: mockData.token,
        });
    });

    test('should respond with a validation error on invalid input', async () => {
        const req = mockRequest();
        const res = mockResponse();
        const requestBody = { ...mockData.requestBody, password: 12345678 };
        req.body = requestBody;

        // Assuming validation function returns an error object for invalid input
        mockGenerateToken.mockImplementation(() => { });
        mockValidationService.mockReturnValue(mockData.passwordValidationError);
        (findUserQuery as jest.Mock).mockResolvedValue(null);
        (createUserQuery as jest.Mock).mockResolvedValue(null);

        await signup(req, res);

        // Assert whether the dependency functions has been called
        expect(mockValidationService).toHaveBeenCalledTimes(1);
        expect(mockGenerateToken).toHaveBeenCalledTimes(0);

        // Assert signup function response
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            message: '.password should be string',
        });
    });

    test('should respond with a server error on exception', async () => {
        const req = mockRequest();
        const res = mockResponse();
        req.body = mockData.requestBody;

        // Mocking an exception
        mockGenerateToken.mockImplementation(() => { throw new Error('Error while generating token'); });
        mockValidationService.mockReturnValue({});
        (findUserQuery as jest.Mock).mockResolvedValue(null);
        (createUserQuery as jest.Mock).mockResolvedValue(mockData.createQueryResponse);

        await signup(req, res);

        // Assert whether the dependency functions has been called
        expect(mockValidationService).toHaveBeenCalledTimes(1);
        expect(mockGenerateToken).toHaveBeenCalledTimes(1);

        // Assert signup function response
        expect(mockGenerateToken).toThrow();
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.json).toHaveBeenCalledWith({
            message: 'Server error',
        });
    });

    test('should respond with an email already exists', async () => {
        const req = mockRequest();
        const res = mockResponse();
        req.body = mockData.requestBody;

        // Mocking an exception
        mockValidationService.mockReturnValue({});
        (findUserQuery as jest.Mock).mockResolvedValue(mockData.findQueryResponse);

        await signup(req, res);

        // Assert whether the dependency functions has been called
        expect(mockValidationService).toHaveBeenCalledTimes(1);
        expect(mockGenerateToken).toHaveBeenCalledTimes(0);

        // Assert signup function response
        expect(res.status).toHaveBeenCalledWith(404);
        expect(res.json).toHaveBeenCalledWith({
            message: 'Email already exists!',
        });
    });

    test('should respond with password mismatch error', async () => {
        const req = mockRequest();
        const res = mockResponse();
        const requestBody = { ...mockData.requestBody };
        requestBody.password = "password_mismatch";
        req.body = requestBody;

        // Mocking an exception
        mockValidationService.mockReturnValue({});

        await signup(req, res);

        // Assert whether the dependency functions has been called
        expect(mockValidationService).toHaveBeenCalledTimes(1);
        expect(mockGenerateToken).toHaveBeenCalledTimes(0);

        // Assert signup function response
        expect(res.status).toHaveBeenCalledWith(400);
        expect(res.json).toHaveBeenCalledWith({
            message: 'Password is mismatched',
        });
    });
});