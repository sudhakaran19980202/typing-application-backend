import bcrypt from "bcryptjs";

export async function bcryptCompare (givenPassword: string, userPassword: string): Promise<boolean> {
    try {
        const isMatch = await bcrypt.compare(givenPassword, userPassword);
        return isMatch;
    } catch (error){
        return false;
    }
}