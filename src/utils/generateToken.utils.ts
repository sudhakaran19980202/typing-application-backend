import jwt from "jsonwebtoken";
import { Types } from "mongoose";

const generateToken = (userId: Types.ObjectId) => {
    const token: string = jwt.sign(
        {
            exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24), // one day validity
            _id: userId,
            role: 'USER',
        },
        process.env['SECRET_KEY']
    );

    return token;
};

export default generateToken;
