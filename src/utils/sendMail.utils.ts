import nodemailer from 'nodemailer';

async function sendMail (invitationEmail: string): Promise<void> {
    try {

        // Create a Nodemailer transporter
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env['SENDER_MAIL'], 
                pass: process.env['APP_PASS'], 
            },
        });

        // Email content
        const mailOptions = {
            from: process.env['SENDER_MAIL'], 
            to: invitationEmail, 
            subject: 'Invitation to Typing Application',
            html: `
        <p>Hello!</p>
        <p>You are invited to join Typing Application. Click the link below to get started:</p>
        <a href="https://typing-application-frontend.vercel.app/">Join My Typing Application</a>
        <p>Thanks!</p>
      `,
        };

        // Send the email
        await transporter.sendMail(mailOptions);
    } catch (error) {
        console.error('Error sending invitation email:', error);
    }
}

export {sendMail};