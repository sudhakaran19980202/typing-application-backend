import SchemaValidator from "easy-schema-validator";

const validation = async (schema, input) => {
    const validator = new SchemaValidator(schema);
    const errors = await validator.validate(input);

    if (errors.length !== 0) {
        return errors;
    }

    return {};
};

export default validation;