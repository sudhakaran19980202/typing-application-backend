// Env config
import "../../config/environment.config";

// Seeder Library
import mongoose from "mongoose";

// Db Config
import MONGO_URL from "../../config/db.config";

// Models
import User from "../../models/users.model";
import Score from "../../models/scores.model";
import Subscription from "../../models/subscriptions.model";
import Word from "../../models/words.model";

// Seed Json Data
import seedData from "./data/data.json";

const seed = async () => {
    try {
    // Connect to MongoDB via Mongoose
        mongoose.connect(MONGO_URL);

        // Delete the existing records
        await User.deleteMany({});
        await Score.deleteMany({});
        await Subscription.deleteMany({});
        await Word.deleteMany({});

        // Seed the data to the collections
        await User.insertMany(seedData.users);
        await Score.insertMany(seedData.scores);
        await Subscription.insertMany(seedData.subscriptions);
        await Word.insertMany(seedData.words);

        // Disconnect the db
        mongoose.disconnect();
    } catch (err) {
        console.log(err);
    }
};

seed();
