
// Model
import Score from "../../models/scores.model";


// List scores query based on user id and page limit
const findScoresQuery = async (userId: string, pageLimit: number) => {
    const scores = await Score.find({ userId }).sort({ createdAt: -1 }).limit(pageLimit).exec();
    return scores;
};

// Create score query based on user id
const createScoreQuery = async (userId, data) => {
    await Score.create({
        userId: userId, ...data,
    });
};

// List leaderboards based on score cards
const getLeaderboardQuery = async (pageLimit: number) => {
    const leaderboardDetails = await Score.aggregate([
        { $sort: { wpm: -1 } },
        { $limit: pageLimit },
        {
            $lookup: {
                from: "users",
                localField: "userId",
                foreignField: "_id",
                as: "userId",
            },
        },
        { $unwind: "$userId" },
        {
            $project: {
                _id: 1,
                userId: {
                    firstName: 1,
                    lastName: 1,
                },
                wpm: 1,
                cpm: 1,
                accuracy: 1,
            },
        },
    ]);

    return leaderboardDetails;
};

export {
    findScoresQuery,
    createScoreQuery,
    getLeaderboardQuery,
};