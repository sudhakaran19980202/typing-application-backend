// Models
import User from "../../models/users.model";

// Find an user by email id
const findUserQuery = async (email: string) => {
    const existingUser = await User.findOne({ email });
    return existingUser;
};

// Create an user
const createUserQuery = async (data, passwordHash) => {
    const user = await User.create({
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        password: passwordHash,
        bio: data.bio,
    });
    return user;
};

// Update an existing user
const updateUserQuery = async (user, data) => {
    await User.updateOne(
        { _id: user._id },
        {
            firstName: data.firstName || user.firstName,
            lastName: data.lastName || user.lastName,
            bio: data.bio || user.bio,
            profileUrl: data.profileUrl || user.profileUrl,
        }
    );
};

export {
    findUserQuery,
    createUserQuery,
    updateUserQuery,
};