// Model
import Word from "../../models/words.model";


// Take all words and use distinct (unique words)
const findAllWordsQuery = async () => {
    const words = await Word.find().distinct('word');
    return words;
};

// Take a sample words; Sample Limit is 100;
const findWordQuery = async (sampleLimit: number) => {
    const words = await Word.aggregate([
        { $sample: { size: sampleLimit } },
        { $group: { _id: null, words: { $push: "$word" } } },
        {
            $project: {
                _id: 0,
                words: 1,
            },
        },
    ]);
    return words[0].words;
};

// Create or migrate a word list
const createWordsQuery = async (uniqueNewWords: string[]) => {
    await Word.insertMany(uniqueNewWords.map((word: string) => ({ word })));
};

export {
    findAllWordsQuery,
    findWordQuery,
    createWordsQuery,
};