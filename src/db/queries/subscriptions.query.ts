// Model
import Subscription from "../../models/subscriptions.model";

// Check whether the email is already subscribed or not
const findSubscriptionQuery = async (email: string) => {
    const existingEmail = await Subscription.findOne({ email });
    return existingEmail;
};

// Create a subscription by using email id
const createSubscriptionQuery = async (email: string) => {
    await Subscription.create({ email });
};
export {
    findSubscriptionQuery,
    createSubscriptionQuery,
};