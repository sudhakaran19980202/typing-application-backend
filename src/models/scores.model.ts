import mongoose from "mongoose";

const Score = new mongoose.Schema({
    userId: {
        type: mongoose.Types.ObjectId,
        ref: "users",
        required: true,
    },
    wpm: {
        type: Number,
        required: true,
    },
    cpm: {
        type: Number,
        required: true,
    },
    accuracy: {
        type: Number,
        required: true,
    },
    type: {
        type: String,
        enum: ["Rabbit", "Turtle", "Sloth"],
    },
}, { timestamps: true });

export = mongoose.model("scores", Score)