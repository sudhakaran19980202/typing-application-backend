import mongoose from "mongoose";

const Word = new mongoose.Schema({
    word: {
        type: String,
        required: true,
    },
}, { timestamps: true });

export = mongoose.model("words", Word)