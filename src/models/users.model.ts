import mongoose from "mongoose";

const User = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    profileUrl: {
        type: String,
        required: false,
    },
    bio: {
        type: String,
        required: false,
    },
    isEnabled: {
        type: Boolean,
        default: true,
    },
}, { timestamps: true });

export = mongoose.model("users", User);