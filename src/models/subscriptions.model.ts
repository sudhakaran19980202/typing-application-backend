import mongoose from "mongoose";

const Subscription = new mongoose.Schema({
    email: {
        type: String,
        required: true,
    },
}, { timestamps: true });

export = mongoose.model("subscriptions", Subscription)