# Use the official Node.js image with Alpine Linux as the base
FROM node:alpine

# Set the working directory inside the container
WORKDIR /usr/typing-application-backend

# Copy the rest of the application files excluding that mentioned in the .dockerignore file
COPY . .

# Install dependencies and take the typescript build
RUN yarn && yarn build

# Expose the port
EXPOSE 5000

# Define the command to run the application
ENTRYPOINT ["yarn", "start"]
